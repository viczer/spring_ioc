package org.eclipse.model;

import java.util.List;

public class Personne {

	private int id;
	private String nom;
	private Adresse adresse;
	private List<String> sports;

	public Personne() {
		super();
		// TODO Auto-generated constructor stub
	}


	public Personne(int id, String nom, Adresse adresse, List<String> sports) {
		super();
		this.id = id;
		this.nom = nom;
		this.adresse = adresse;
		this.sports = sports;
	}


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public Adresse getAdresse() {
		return adresse;
	}

	public void setAdresse(Adresse adresse) {
		this.adresse = adresse;
	}

	

	@Override
	public String toString() {
		return "Personne [id=" + id + ", nom=" + nom + ", adresse=" + adresse + ", sports=" + sports + "]";
	}


	public void afficher() {

		System.out.println("Personne id=" + id + "   nom :"+nom + "\n" + adresse + sports);
//		System.out.println("Mes sport :");
//		sports.forEach(System.out::println);
	}


//	public List<String> getSports() {
//		return sports;
//	}
//
//
//	public void setSports(List<String> sports) {
//		this.sports = sports;
//	}

}
